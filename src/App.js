import React, { useEffect, useState } from 'react';
import './App.css';
import Header from './layouts/Header.js';
import Timer from './components/Timer.js';

function App() {
  return (
    <div className="App">
      <Header />
      <Timer />
    </div>
  );
}

export default App;
