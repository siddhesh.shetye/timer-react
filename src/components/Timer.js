import React, { useState, useEffect } from 'react';
import Swal from 'sweetalert2';

const Timer = () => {
    const [counter, setCounter] = useState(0);
    const [isRunning, setRunning] = useState(false);

    useEffect(() => {

        if (isRunning && counter > 0) {
            setTimeout(() => setCounter(counter - 1), 1000);
        } else if (isRunning && counter == 0) {
            setRunning(false);
        }

    }, [isRunning, counter]);

    const handleInputChange = e => {
        setCounter(e.target.value);
    }

    function resetTimer() {
        if (counter > 0) {
            setCounter(0);
            setRunning(false);
        }
    }

    function toggleRunning() {
        if (counter > 0 && Number.isInteger(parseInt(counter))) {
            setRunning(!isRunning);
        } else {
            Swal.fire({
                icon: 'error',
                text: 'Seconds must be a digit and greater then 0',
                confirmButtonText: 'Cool',
                width: 600,
                padding: '3em',
                background: '#fff',
                backdrop: 'rgba(0,0,123,0.4) url("/assets/nyan-cat.gif") left top no-repeat'
            })
        }
    }

    return (
        <div className="container pb-30">
            <div className="grid-margin stretch-card">
                <div className="card">
                    <div className="row justify-content-center">
                        <div className="col-md-4 mt-4">
                            <div className="bg-secondary px-4 py-4 card">
                                <input type="text" className="form-control bg-secondary border-none text-white text-center" placeholder="enter seconds"
                                    name="counter"
                                    onChange={handleInputChange}
                                    value={counter} />
                            </div>
                        </div>
                    </div>

                    <div className="card-body">
                        <button className="btn btn-primary m-10" onClick={toggleRunning}>
                            {!isRunning ? 'Start' : 'Pause'}
                        </button>

                        <button className="btn btn-danger m-10" onClick={resetTimer}>
                            Reset
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Timer;